﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

[System.Serializable]
public struct SpawnData
{
    public int xPos;
    public BaseColours spnColour;
}

public class BlockSpawnPositions : MonoBehaviour {

    public static BlockSpawnPositions _curBlockSpawnPositions { get; private set; }

    public int spawnDataPoolSize = 400, curRowPos = 0;
    List<List<SpawnData>> levelSpawnData;
    BaseColours tempColourRef;

    public BlockSpawnData masterSpawnDataObject;

    private void Awake()
    {
        if (_curBlockSpawnPositions != null && _curBlockSpawnPositions != this)
        {
            Destroy(gameObject);
        }
        else
        {
            _curBlockSpawnPositions = this;
            DontDestroyOnLoad(gameObject);
        }
    }

    private void Start()
    {
        GenerateLevelData(Environment.TickCount);
        curRowPos = UnityEngine.Random.Range(0, spawnDataPoolSize);
    }

    public void GenerateLevelData(int rndSeed)
    {
        
        UnityEngine.Random.InitState(rndSeed);

        levelSpawnData = new List<List<SpawnData>>();

        for (int i = 0; i < spawnDataPoolSize; i++)
        {
            int blocksInRow = UnityEngine.Random.Range(1, 5);

            List<SpawnData> tempDataList = new List<SpawnData>();

            for (int x = 0; x < blocksInRow; x++)
            {
                SpawnData sd = new SpawnData();
                int tempXPos = GenerateSpawnPosition();
                while (tempDataList.Exists(p => p.xPos == tempXPos))
                {
                    tempXPos = GenerateSpawnPosition();
                }
                sd.xPos = tempXPos;
                //sd.spnColour = GenerateSpawnColour();
                BaseColours c = GenerateSpawnColour();
                bool pickNewColour = c == tempColourRef ? true : false;
                int colourCount = 0;
                while (pickNewColour == true)
                {
                    c = GenerateSpawnColour();
                    pickNewColour = c == tempColourRef ? true : false;
                    colourCount++;
                    if(colourCount >= 2)
                    {
                        pickNewColour = false;
                    }
                }
                sd.spnColour = c;
                tempColourRef = c;

                tempDataList.Add(sd);
            }
            
            levelSpawnData.Add(tempDataList);
        }
        
    }

    int GenerateSpawnPosition()
    {
        int xPos = UnityEngine.Random.Range(-2, 3);

        return xPos;
    }

    BaseColours GenerateSpawnColour()
    {
        int rndCol = UnityEngine.Random.Range(0, 3);
        BaseColours C = BaseColours.Red;
        switch (rndCol)
        {
            case 0:
                //DO NOTHING
                break;
            case 1:
                C = BaseColours.Green;
                break;
            case 2:
                C = BaseColours.Blue;
                break;
            default:
                break;
        }
        
        return C;
    }

    public int GetRowCount()
    {
        return levelSpawnData[curRowPos].Count;
    }

    public SpawnData ReturnNextBlock(int rowPos)
    {
        return levelSpawnData[curRowPos][rowPos];
    }

    public void AdvanceRowCount()
    {
        curRowPos++;
        if (curRowPos >= levelSpawnData.Count)
        {
            curRowPos = 0;
        }
    }
}

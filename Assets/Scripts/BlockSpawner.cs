﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BlockSpawner : MonoBehaviour {

    public static BlockSpawner curBlockSpawner { get; private set; }

    public GameObject blockPrefab, lootBlock;
    public int blockPoolSize, lootBlocksToPool, blocksToSpawnBeforeSpeedUp;
    public float blockSpawnInterval, blockSpawnMinDistance;
    public Vector3 spawnPosZero;

    List<GameObject> blockPool, lootBlockPool;
    float lastXPos = 2.0f, nextBlockMoveSpeed, nextSpawnDist;
    int redCount = 0, greenCount = 0, blueCount = 0, blockCount = 0, maxBlocksToSpawn = 1, countUntilLootBlockSpawn;
    Transform lastSpawnedBlock;

    public int MaxBlocksToSpawn
    {
        get { return maxBlocksToSpawn; }
        set { maxBlocksToSpawn = value; maxBlocksToSpawn = Mathf.Clamp(maxBlocksToSpawn, 1, 4); }
    }

    public float NextBlockMoveSpeed
    {
        get { return nextBlockMoveSpeed; }
        set { nextBlockMoveSpeed = value; }
    }

    public float NextSpawnDist
    {
        set { nextSpawnDist = value; }
    }

    private void Awake()
    {
        if (curBlockSpawner != null && curBlockSpawner != this)
        {
            Destroy(gameObject);
        }
        else
        {
            curBlockSpawner = this;
            DontDestroyOnLoad(gameObject);
        }

        blockPool = new List<GameObject>();

        for (int i = 0; i < blockPoolSize; i++)
        {
            GameObject obj = (GameObject)Instantiate(blockPrefab);
            obj.SetActive(false);
            blockPool.Add(obj);
        }

        lootBlockPool = new List<GameObject>();

        for (int i = 0; i < lootBlocksToPool; i++)
        {
            GameObject obj = (GameObject)Instantiate(lootBlock);
            obj.SetActive(false);
            lootBlockPool.Add(obj);
        }
    }

    // Use this for initialization
    void Start () {
        
        blockCount = 0;
        maxBlocksToSpawn = 1;
        countUntilLootBlockSpawn = GenerateRandomInt(20, 30);
        //SpawnNewBlock();
    }

    // Update is called once per frame
    //void Update () {

    //       if(lastSpawnedBlock.position.y <= transform.position.y - nextSpawnDist)
    //       {
    //           //SpawnNewBlock();
    //           SpawnDropBlock();
    //       }

    //}

    internal void SpawnDropBlock()
    {
        if (blockCount >= blocksToSpawnBeforeSpeedUp)
        {
            blockCount = 0;
            GameManager.curGameManager.UpdateLevelSpeed();
            //BlockSpawner.curBlockSpawner.MaxBlocksToSpawn++;
        }

        int blocksToSpawn = BlockSpawnPositions._curBlockSpawnPositions.GetRowCount();

        for (int x = 0; x < blocksToSpawn - 1; x++)
        {
            countUntilLootBlockSpawn--;
            SpawnData data = BlockSpawnPositions._curBlockSpawnPositions.ReturnNextBlock(x);
            if(countUntilLootBlockSpawn <= 0)
            {
                SpawnBlock(data, true);
                countUntilLootBlockSpawn = GenerateRandomInt(40, 160);
            }
            else
            {
                SpawnBlock(data);
            }
            
            blockCount++;
            
        }

        BlockSpawnPositions._curBlockSpawnPositions.AdvanceRowCount();
    }

    void SpawnBlock(SpawnData spnData, bool isLootBlock = false)
    {
        if(isLootBlock == false)
        {
            for (int i = 0; i < blockPool.Count; i++)
            {
                if (!blockPool[i].activeInHierarchy)
                {
                    Vector3 _spnPosition = spawnPosZero;
                    _spnPosition.x = spnData.xPos;
                    blockPool[i].transform.position = _spnPosition;
                    blockPool[i].transform.rotation = Quaternion.identity;
                    //blockPool[i].GetComponent<BlockColour>().ThisBlockColour = spnData.spnColour;
                    blockPool[i].GetComponent<UvColourChanger>().EntityColour = spnData.spnColour;
                    BlockMover bm = blockPool[i].GetComponent<BlockMover>();
                    bm.MoveSpeed = nextBlockMoveSpeed;
                    blockPool[i].GetComponent<BlockMover>().MoveSpeed = nextBlockMoveSpeed;
                    blockPool[i].SetActive(true);
                    lastSpawnedBlock = blockPool[i].transform;
                    return;
                }
            }
        }
        else
        {
            for (int i = 0; i < lootBlockPool.Count; i++)
            {
                if(!lootBlockPool[i].activeInHierarchy)
                {
                    Vector3 _spnPosition = spawnPosZero;
                    _spnPosition.x = spnData.xPos;
                    lootBlockPool[i].transform.position = _spnPosition;
                    lootBlockPool[i].transform.rotation = Quaternion.identity;
                    BlockMover bm = blockPool[i].GetComponent<BlockMover>();
                    bm.MoveSpeed = nextBlockMoveSpeed;
                    lootBlockPool[i].GetComponent<BlockMover>().MoveSpeed = nextBlockMoveSpeed;
                    lootBlockPool[i].SetActive(true);
                    lastSpawnedBlock = lootBlockPool[i].transform;
                    return;
                }
            }
        }
        
    }

    internal void SpawnNewBlock()
    {
        int _s = Random.Range(1, maxBlocksToSpawn);
        int _count = 0;

        if (blockCount >= blocksToSpawnBeforeSpeedUp)
        {
            blockCount = 0;
            GameManager.curGameManager.UpdateLevelSpeed();
            //BlockSpawner.curBlockSpawner.MaxBlocksToSpawn++;
        }

        for (int i = 0; i < blockPool.Count; i++)
        {
            if(!blockPool[i].activeInHierarchy)
            {
                blockPool[i].transform.position = PickSpawnPosition();
                blockPool[i].transform.rotation = Quaternion.identity;
                BaseColours newColour = PickNewBlockColour();
                //blockPool[i].GetComponent<EntityColour>().SetEntityColour(newColour);
                //blockPool[i].GetComponent<BlockColour>().ThisBlockColour = newColour;
                blockPool[i].GetComponent<UvColourChanger>().EntityColour = newColour;
                BlockMover bm = blockPool[i].GetComponent<BlockMover>();
                bm.MoveSpeed = nextBlockMoveSpeed;
                //bm.MyBaseColour = newColour;
                blockPool[i].GetComponent<BlockMover>().MoveSpeed = nextBlockMoveSpeed;
                blockPool[i].SetActive(true);
                lastSpawnedBlock = blockPool[i].transform;
                blockCount++;

                _count++;
                if (_count >= _s)
                {
                    return;
                }
            }
        }

        
    }

    BaseColours PickNewBlockColour()
    {
        //BaseColours bc = BaseColours.Red;

        int rnd = Random.Range(0, 3);
        if(rnd == 0 && redCount > 0)
        {
            rnd = Random.Range(0, 2) == 0 ? 1 : 2;
        }
        else if (rnd == 1 && greenCount > 0)
        {
            rnd = Random.Range(0, 2) == 0 ? 0 : 2;
        }
        else if (rnd == 2 && blueCount > 0)
        {
            rnd = Random.Range(0, 2) == 0 ? 0 : 1;
        }
        switch (rnd)
        {
            case 0:
                redCount++;
                greenCount = 0;
                blueCount = 0;
                //bc = BaseColours.Red;
                return BaseColours.Red;
            case 1:
                redCount = 0;
                greenCount++;
                blueCount = 0;
                //bc = BaseColours.Green;
                return BaseColours.Green;
            case 2:
                redCount = 0;
                greenCount = 0;
                blueCount++;
                //bc = BaseColours.Blue;
                return BaseColours.Blue;

        }
        
        return BaseColours.Red;
    }

    Vector3 PickSpawnPosition()
    {
        Vector3 spnPos = spawnPosZero;
        int rnd = Random.Range(0, 5);
        switch (rnd)
        {
            case 0:
                spnPos.x = -2.0f;
                break;
            case 1:
                spnPos.x = -1.0f;
                break;
            case 2:
                //do nothing - spnPos already set
                break;
            case 3:
                spnPos.x = 1.0f;
                break;
            case 4:
                spnPos.x = 2.0f;
                break;
            default:
                break;
        }

        if(lastXPos == spnPos.x)
        {
            float offset = 0.0f;

            if (spnPos.x >= -1.0f && spnPos.x <= 1.0f)
            {
                offset = Random.Range(0, 2) == 0 ? -1.0f : 1.0f;
            }
            else
            {
                offset = spnPos.x < -1.0f ? 1.0f : -1.0f;
            }

            spnPos.x += offset;

        }
        
        lastXPos = spnPos.x;
        return spnPos;
    }

    public void UpdateActiveBlocksSpeed()
    {
        for (int i = 0; i < blockPool.Count; i++)
        {
            if(blockPool[i].activeInHierarchy)
            {
                blockPool[i].GetComponent<BlockMover>().MoveSpeed = NextBlockMoveSpeed;
            }
        }
    }

    public void ClearScreen()
    {
        for (int i = 0; i < blockPool.Count; i++)
        {
            if(blockPool[i].activeInHierarchy == true)
            {
                blockPool[i].SetActive(false);
            }
        }
        for (int i = 0; i < lootBlockPool.Count; i++)
        {
            if (lootBlockPool[i].activeInHierarchy == true)
            {
                lootBlockPool[i].SetActive(false);
            }
        }
    }

    int GenerateRandomInt(int lowerLimit, int upperLimit)
    {
        return Random.Range(lowerLimit, upperLimit);
    }
}

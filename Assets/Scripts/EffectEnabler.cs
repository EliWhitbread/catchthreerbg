﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EffectEnabler : MonoBehaviour
{
    ParticleSystem pSystem;
    ParticlesMover pMover;
    Transform pTransform;
    bool isActive = false, hasParticleMover = false;

    public bool IsActive
    {
        get { return isActive; }
    }

    public void InitParticles()
    {
        pSystem = transform.gameObject.GetComponent<ParticleSystem>();
        pTransform = this.transform;
        pMover = gameObject.GetComponent<ParticlesMover>();
        if(pMover != null)
        {
            pMover.enabled = false;
            hasParticleMover = true;
        }
    }

    public void Play()
    {
        pSystem.Play();
        StartCoroutine(WaitToDisable());
        isActive = true;
        if (hasParticleMover == true)
        {
            pMover.enabled = true;
        }
    }

    IEnumerator WaitToDisable()
    {
        while(pSystem.IsAlive())
        {
            yield return null;
        }
        isActive = false;
        pTransform.position = Vector3.right * 20;
        if (hasParticleMover == true)
        {
            pMover.TargetPlayerPos = false;
            pMover.enabled = false;
        }
    }

    public void Stop()
    {
        isActive = false;
        pTransform.position = Vector3.right * 20;
        if (hasParticleMover == true)
        {
            pMover.TargetPlayerPos = false;
            pMover.enabled = false;
        }
        pSystem.Stop();
        pSystem.Clear();
    }
}

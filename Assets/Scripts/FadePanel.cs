﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class FadePanel : MonoBehaviour
{
    GameObject fadePanel;
    Image fadeImage;
    Color fadeColour;
    float fadeInTime = 5.0f, fadeOutTime = 1.0f;
    bool isFading = false;
    TextMeshProUGUI deathMessagetext;

    private void OnEnable()
    {
        if(fadePanel == null)
        {
            fadePanel = GameObject.FindGameObjectWithTag("FadePanel");
            fadeImage = fadePanel.GetComponent<Image>();
            
        }

        deathMessagetext = fadePanel.GetComponentInChildren<TextMeshProUGUI>();

        fadeColour = fadeImage.color;
        fadeColour.a = 0.0f;
        SetColour();        
    }

    private void Start()
    {
        StartCoroutine(WaitForGameManager());
    }

    IEnumerator WaitForGameManager()
    {
        yield return new WaitUntil(() =>GameManager.curGameManager != null);
        GameManager.curGameManager.SetFadePanel = this;

        fadePanel.SetActive(false);
    }

    public void Fade()
    {
        fadePanel.SetActive(true);
        deathMessagetext.text = "";

        if(isFading == true)
        {
            StopCoroutine("FadeInOut");
        }
        
        StartCoroutine("FadeInOut");
    }

    IEnumerator FadeInOut()
    {
        deathMessagetext.text = GameManager.curGameManager.uiManager.Message;
        isFading = true;
        float _a = 0.0f;
        fadeColour.a = _a;
        SetColour();

        while (_a < 1.0f)
        {
            _a += fadeInTime * Time.deltaTime;
            fadeColour.a = _a;
            SetColour();
            yield return null;
        }
        yield return new WaitForSeconds(0.5f);
        while (_a > 0.0f)
        {
            _a -= fadeOutTime * Time.deltaTime;
            fadeColour.a = _a;
            SetColour();
            yield return null;
        }

        isFading = false;
        fadePanel.SetActive(false);
    }

    void SetColour()
    {
        fadeImage.color = fadeColour;
    }
}

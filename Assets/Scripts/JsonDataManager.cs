﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;

public class JsonDataManager : MonoBehaviour
{
    public static JsonDataManager dataManager { get; private set; }
    public PlayerSaveData playerSaveData;
    string dataPath;

    private void Awake()
    {
        if(dataManager != null && dataManager != this)
        {
            Destroy(this);
        }
        else
        {
            dataManager = this;
            DontDestroyOnLoad(this.gameObject);
        }
    }

    private void Start()
    {
        GetDataPath();
    }

    void GetDataPath()
    {
        dataPath = Path.Combine(Application.persistentDataPath, "PlayerData.txt");
    }

    public void SavePlayerData(PlayerSaveData data)
    {
        if(dataPath == null)
        {
            GetDataPath();
        }

        string jsonString = JsonUtility.ToJson(data);
        using(StreamWriter streamWriter = File.CreateText(dataPath))
        {
            streamWriter.Write(jsonString);
        }
    }

    public PlayerSaveData LoadPlayerData()
    {
        if (dataPath == null)
        {
            GetDataPath();
        }

        using (StreamReader streamReader = File.OpenText(dataPath))
        {
            string jsonString = streamReader.ReadToEnd();
            return JsonUtility.FromJson<PlayerSaveData>(jsonString);
        }
    }


}

[System.Serializable]
public struct PlayerSaveData{

    public string playerName;
    public int lastScore, bestScore;
    public int lootBlockCount;
    public float lastGameTime, bestGameTime;

}
﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

enum Direction { Left, Right }

struct MoveDirection
{
    public Direction _direction;
}

public class PlayerMover : MonoBehaviour {

    public static PlayerMover curPlayerMover { get; private set; }

    float moveDuration;

    Queue<MoveDirection> moveQueue;
    bool isMoving, falling;
    EntityColour myEntityColour;

    Transform playerTransform;
    Vector3 nexMoveDir = Vector3.zero;
    Vector3 rotAxis,pivot;
    Quaternion startRotation, endRotation;
    Vector3 startPosition, endPosition;

    float rotSpeed, t;
    int lyrMask;

    CameraMover camMover;


    public float MoveDuration
    {
        get { return moveDuration; }
        set { moveDuration = value; }
    }

    private void Awake()
    {
        if(curPlayerMover != null && curPlayerMover != this)
        {
            Destroy(gameObject);
        }
        else
        {
            curPlayerMover = this;
        }

        myEntityColour = gameObject.GetComponent<EntityColour>();
        playerTransform = this.transform;
        //ResetPlayer();
    }

    // Use this for initialization
    void OnEnable () {

        if(playerTransform == null)
        {
            playerTransform = this.transform;
        }
        StartCoroutine(SubscribeToEvents());
        lyrMask = 1 << LayerMask.NameToLayer("Platform");
        //ResetPlayer();
        if(camMover == null)
        {
            camMover = Camera.main.GetComponent<CameraMover>();
            camMover.PlayerTransform = this.transform;
        }
        camMover.ResetPosition();
    }
    

    IEnumerator SubscribeToEvents()
    {
        yield return new WaitUntil(() => InputManager.curInputManager != null);
        
        InputManager.curInputManager._moveLeft += CurInputManager__moveLeft;
        InputManager.curInputManager._moveRight += CurInputManager__moveRight;        
    }

    private void CurInputManager__moveRight()
    {
        MoveDirection dir = new MoveDirection();
        dir._direction = Direction.Right;
        moveQueue.Enqueue(dir);
    }

    private void CurInputManager__moveLeft()
    {
        MoveDirection dir = new MoveDirection();
        dir._direction = Direction.Left;
        moveQueue.Enqueue(dir);
    }

    public void ResetPlayer()
    {
        moveQueue = new Queue<MoveDirection>();
        isMoving = false;
        falling = false;
        playerTransform.position = Vector3.zero;
        playerTransform.rotation = Quaternion.identity;
        camMover.ResetPosition();
    }

    // Update is called once per frame
    void Update () {
		
        if(isMoving == false)
        {
            try
            {
                Direction dir = moveQueue.Dequeue()._direction;
                switch (dir)
                {
                    case Direction.Left:
                        isMoving = true;
                        nexMoveDir = Vector3.left;
                        SetNextMove();
                        //StartCoroutine(MovePlayer(Vector3.left));
                        break;
                    case Direction.Right:
                        isMoving = true;
                        nexMoveDir = Vector3.right;
                        SetNextMove();
                        //StartCoroutine(MovePlayer(Vector3.right));
                        break;
                    default:
                        break;
                }
                camMover.FollowPlayer = true;
            }
            catch
            {
                //do nothing
            }

            return;
            
        }
        else if(falling == false)
        {
            
            t += Time.deltaTime;
            playerTransform.RotateAround(pivot, rotAxis, rotSpeed * Time.deltaTime);                

            if(t >= moveDuration || playerTransform.position == endPosition)
            {
                RaycastHit hit;
                Vector3 rayStartPos = playerTransform.position;
                rayStartPos.y += 2.0f;
                if (Physics.Raycast(rayStartPos, Vector3.down, out hit, 100.0f, lyrMask))
                {
                    playerTransform.position = endPosition;
                    playerTransform.rotation = endRotation;

                    isMoving = false;
                    camMover.FollowPlayer = false;
                }
                else if(hit.collider == null)
                {
                    falling = true;
                }

            }
            
        }
        if(falling == true)
        {
            camMover.FollowPlayer = false;
            playerTransform.Translate(Vector3.down * 3.0f * Time.deltaTime, Space.World);
            playerTransform.RotateAround(playerTransform.position, rotAxis, 50.0f * Time.deltaTime);
            if (playerTransform.position.y < -3.0f)
            {
                GameManager.curGameManager.PlayerDied(true);
                gameObject.SetActive(false);
            }
        }
	}

    private void SetNextMove()
    {
        rotAxis = Vector3.Cross(Vector3.up, nexMoveDir);
        pivot = (playerTransform.position + Vector3.down * 0.5f) + nexMoveDir * 0.5f;

        startRotation = playerTransform.rotation;
        endRotation = Quaternion.AngleAxis(90.0f, rotAxis) * startRotation;

        startPosition = playerTransform.position;
        endPosition = playerTransform.position + nexMoveDir;
        endPosition.y = 0.0f;
        endPosition.z = 0.0f;

        rotSpeed = 90.0f / moveDuration;
        t = 0.0f;
    }
    

    private void OnTriggerEnter(Collider other)
    {
        if(other.CompareTag("LootBlock"))
        {
            EffectSpawner.curEffectSpawner.SpawnEffect(SpawnableEffects.LootBlockDeath, other.transform, BaseColours.Red, true);
            GameManager.curGameManager.CurrentLootBlocks = 1;
            //GameManager.curGameManager.sc
            other.gameObject.SetActive(false);
            return;
        }
        //EntityColour colrChanger = other.GetComponent<EntityColour>();
        //BlockColour otherColour = other.GetComponent<BlockColour>();
        UvColourChanger otherColour = other.GetComponent<UvColourChanger>();
        if (otherColour != null && other.gameObject.activeInHierarchy == true)
        {
            //myEntityColour.AddEntityColours(colrChanger.ObjectColour);
           // Debug.Log("Enemy Colour Enum:" + otherColour.ThisBlockColour.ToString() + " Enemy Colour Actual: " + otherColour.gameObject.GetComponent<MeshRenderer>().material.color.ToString()
            //    + " Player Colour Enum: " + myEntityColour.playerColour.ToString() + " Player Colour Actual: " + myEntityColour.ObjectColour.ToString());
            myEntityColour.CombineEntityColours(otherColour.EntityColour);
            EffectSpawner.curEffectSpawner.SpawnEffect(SpawnableEffects.DropBlockDeath, other.transform, otherColour.EntityColour, true);
            otherColour.gameObject.SetActive(false);
        }

    }


    private void OnDisable()
    {
        InputManager.curInputManager._moveLeft -= CurInputManager__moveLeft;
        InputManager.curInputManager._moveRight -= CurInputManager__moveRight;
        moveQueue.Clear();
        camMover.FollowPlayer = false;
    }
}

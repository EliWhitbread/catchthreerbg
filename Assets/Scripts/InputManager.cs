﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InputManager : MonoBehaviour {

    public static InputManager curInputManager { get; private set; }

    //movement
    public delegate void MovePlayer();
    public event MovePlayer _moveLeft;
    public event MovePlayer _moveRight;
    
    bool touchDetected = false;
    Vector2 firstTouchPos, endTouchPos;

    //[SerializeField]
    //Camera mainCam;

    private void Awake()
    {
        if(curInputManager != null && curInputManager != this)
        {
            Destroy(gameObject);
            return;
        }
        else
        {
            curInputManager = this;
            DontDestroyOnLoad(gameObject);
        }
    }
    
    void Update () {

#if UNITY_IOS || UNITY_ANDROID

        if (Input.touchCount > 0)
        {
            if(Input.touches[0].phase == TouchPhase.Began)
            {
                firstTouchPos = Input.touches[0].position;
            }
            if (Input.touches[0].phase == TouchPhase.Ended || Input.touches[0].phase == TouchPhase.Moved)
            {
                endTouchPos = Input.touches[0].position;

                Vector2 offset = firstTouchPos - endTouchPos;
                float sqrLength = offset.sqrMagnitude;
                if(sqrLength > (20 * 20) && touchDetected == false)
                {
                    touchDetected = true;

                    if (endTouchPos.x < firstTouchPos.x)
                    {
                        if (_moveLeft != null)
                        {
                            _moveLeft();
                        }
                    }
                    else if (endTouchPos.x > firstTouchPos.x)
                    {
                        if (_moveRight != null)
                        {
                            _moveRight();
                        }
                    }
                }
            }
            
        }
        else
        {
            touchDetected = false;
        }

#endif
#if UNITY_STANDALONE || UNITY_EDITOR || UNITY_WEBGL

        if(Input.GetKeyDown(KeyCode.RightArrow) || Input.GetKeyDown(KeyCode.D))
        {
            if(_moveRight != null)
            {
                _moveRight();
            }
        }
        if (Input.GetKeyDown(KeyCode.LeftArrow) || Input.GetKeyDown(KeyCode.A))
        {
            if (_moveLeft != null)
            {
                _moveLeft();
            }
        }
#endif
    }
    
}

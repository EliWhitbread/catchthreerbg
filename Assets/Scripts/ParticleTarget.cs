﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ParticleTarget : MonoBehaviour {

    public GameObject uiTarget;

    private void Start()
    {
        Camera cam = Camera.main;
        Vector3 tPos = uiTarget.GetComponent<RectTransform>().position;
        transform.position = cam.ScreenToWorldPoint(new Vector3(tPos.x, tPos.y, Mathf.Abs(cam.transform.position.z) ));
    }
}

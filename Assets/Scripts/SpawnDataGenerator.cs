﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnDataGenerator : MonoBehaviour {

    public BlockSpawnData blockSpawnDataObject;
    public int dataPoolSize, xPosMin, xPosMax;

    int refXPos = 0;
    BaseColours refColour = BaseColours.Red;

	public void GenerateData()
    {
        blockSpawnDataObject.blockSpawnDataPool = new List<SpawnData>();

        for (int i = 0; i < dataPoolSize; i++)
        {
            SpawnData sd = new SpawnData();
            sd.xPos = GeneratePosition(i == 0? false:true);
            sd.spnColour = GenerateColour();

            blockSpawnDataObject.blockSpawnDataPool.Add(sd);
        }
        
    }

    int GeneratePosition(bool compareToRef = true)
    {
        int xPosition = Random.Range(xPosMin, xPosMax + 1);
        int count = 10;

        if (compareToRef == false || xPosition != refXPos)
        {
            refXPos = xPosition;
            return xPosition;
        }
        while (count > 0)
        {
            xPosition = Random.Range(xPosMin, xPosMax + 1);
            if(xPosition == refXPos)
            {
                count--;
            }
            else
            {
                count = 0;
                break;
            }
        }

        refXPos = xPosition;
        return xPosition;
    }

    BaseColours GenerateColour()
    {
        BaseColours refBaseColour = BaseColours.Red;

        int rnd = Random.Range(0, 3);

        switch (rnd)
        {
            case 0:
                //do nothing
                break;
            case 1:
                refBaseColour = BaseColours.Green;
                break;
            case 2:
                refBaseColour = BaseColours.Blue;
                break;
            default:
                break;
        }
        
        if(refBaseColour != refColour)
        {
            refColour = refBaseColour;
            return refBaseColour;
        }

        int rndm = Random.Range(0, 2);
        switch (refBaseColour)
        {
            case BaseColours.Red:
                if(rndm == 0)
                {
                    refBaseColour = BaseColours.Green;
                }
                else
                {
                    refBaseColour = BaseColours.Blue;
                }
                break;
            case BaseColours.Green:
                if (rndm == 0)
                {
                    refBaseColour = BaseColours.Red;
                }
                else
                {
                    refBaseColour = BaseColours.Blue;
                }
                break;
            case BaseColours.Blue:
                if (rndm == 0)
                {
                    refBaseColour = BaseColours.Green;
                }
                else
                {
                    refBaseColour = BaseColours.Red;
                }
                break;
            default:
                break;
        }
        
        refColour = refBaseColour;
        return refBaseColour;
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EntitySpawner : MonoBehaviour {

    public static EntitySpawner curEntitySpawner { get; private set; }

    public BlockSpawnData blockSpawnDataObject;
    public Vector3 spawnPositionZero;
    [SerializeField]int dataCountOffset;
    bool canSpawn = false;
    public float spawnTimeMin, spawnTimeMax;
    int speedUpSpawnCount = 10, curSpawnCount = 0;

    //drop blocks
    public GameObject dropBlock;
    List<GameObject> dropBlockPool;
    public int initialBlockPoolSize;
    float blockMoveSpeed, lastSpawnPosX = 0;
    int dropBlockScoreValue = 20;

    //pickup
    public GameObject pickUp;
    bool spawnLootblock = false;

    //test
    float spawnDelay, spawnTime = 2.0f;
    int blockCount = 8;

    public int DropBlockScoreValue
    {
        get { return dropBlockScoreValue; }
        set { dropBlockScoreValue = value; }
    }

    public float BlockMoveSpeed
    {
        get { return blockMoveSpeed; }
        set { blockMoveSpeed = value; }
    }

    public bool CanSpawn
    {
        set { canSpawn = value; spawnTime = Random.Range(spawnTimeMin, spawnTimeMax); }
    }

    public bool SpawnLootblock
    {
        set { spawnLootblock = value; }
    }

    private void Awake()
    {
        if(curEntitySpawner != null && curEntitySpawner != this)
        {
            Destroy(this);
        }
        else
        {
            curEntitySpawner = this;
        }

        dataCountOffset = Random.Range(0, blockSpawnDataObject.blockSpawnDataPool.Count - 1);

        InitPools();

        curSpawnCount = 0;
    }

    private void Update()
    {
        if(canSpawn == false)
        {
            return;
        }
        spawnDelay += Time.deltaTime;
        if(spawnDelay >= spawnTime)
        {
            spawnDelay = 0;
            SpawnBlock();
            spawnTime = Random.Range(spawnTimeMin, spawnTimeMax);
            blockCount++;
            if(blockCount >= 10)
            {
                spawnLootblock = true;
                blockCount = 0;
            }
        }
    }

    public void ResetSpawner()
    {
        curSpawnCount = 0;
    }

    public void SpawnBlock()
    {
        dataCountOffset++;
        if (dataCountOffset >= blockSpawnDataObject.blockSpawnDataPool.Count)
        {
            dataCountOffset = (int)Random.Range(0, blockSpawnDataObject.blockSpawnDataPool.Count / 2);
        }

        for (int i = 0; i < dropBlockPool.Count; i++)
        {
            if (!dropBlockPool[i].activeInHierarchy)
            {
                SpawnNext(dropBlockPool[i], blockSpawnDataObject.blockSpawnDataPool[dataCountOffset]);
                return;
            }
        }
        SpawnNext(AddNewBlock(), blockSpawnDataObject.blockSpawnDataPool[dataCountOffset]);
    }

    void SpawnNext(GameObject block, SpawnData data)
    {
        if (data.xPos == lastSpawnPosX)
        {
            lastSpawnPosX = 10.0f;
            return;
        }

        Vector3 _spnPosition = spawnPositionZero;
        _spnPosition.x = data.xPos;

        lastSpawnPosX = _spnPosition.x;

        //spawn loot block
        if(spawnLootblock == true)
        {
            SpawnPickup(_spnPosition);
            return;
        }
        block.transform.position = _spnPosition;
        block.transform.rotation = Quaternion.identity;
        block.GetComponent<UvColourChanger>().EntityColour = data.spnColour;
        BlockMover bm = block.GetComponent<BlockMover>();
        bm.MoveSpeed = blockMoveSpeed;
        block.SetActive(true);

        curSpawnCount++;
        if(curSpawnCount >= speedUpSpawnCount)
        {
            curSpawnCount = 0;
            GameManager.curGameManager.UpdateLevelSpeed();
        }
    }
    
    GameObject AddNewBlock()
    {
        GameObject _block = (GameObject)Instantiate(dropBlock);
        _block.SetActive(false);
        dropBlockPool.Add(_block);

        return _block;
    }

    void SpawnPickup(Vector3 pos)
    {
        GameObject pu = (GameObject)Instantiate(pickUp);
        pu.SetActive(false);
        pu.transform.position = pos;
        BlockMover bm = pu.GetComponent<BlockMover>();
        bm.MoveSpeed = blockMoveSpeed;
        pu.SetActive(true);
        spawnLootblock = false;
    }

    void InitPools()
    {
        dropBlockPool = new List<GameObject>();

        for (int i = 0; i < initialBlockPoolSize; i++)
        {
            GameObject obj = (GameObject)Instantiate(dropBlock);
            obj.SetActive(false);
            dropBlockPool.Add(obj);
        }
    }

    //void UpdateActiveEntitySpeed()
    //{
    //    for (int i = 0; i < dropBlockPool.Count; i++)
    //    {
    //        if(dropBlockPool[i].activeInHierarchy)
    //        {
    //            BlockMover mover = dropBlockPool[i].GetComponent<BlockMover>();
    //            mover.MoveSpeed = blockMoveSpeed;
    //        }
    //    }
    //}

    public void ClearBlocks()
    {
        spawnDelay = 0;

        for (int i = 0; i < dropBlockPool.Count; i++)
        {
            dropBlockPool[i].SetActive(false);
            Destroy(dropBlockPool[i]);
            dropBlockPool[i] = null;
        }

        dropBlockPool.Clear();

        foreach (GameObject pu in GameObject.FindGameObjectsWithTag("LootBlock"))
        {
            Destroy(pu);
        }
    }
}

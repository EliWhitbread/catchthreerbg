﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName ="BlockSpawnData", menuName ="NewSpawnData")]
public class BlockSpawnData : ScriptableObject {

    public string dataHolderName = "Spawn Data";
    public List<SpawnData> blockSpawnDataPool;
	
}

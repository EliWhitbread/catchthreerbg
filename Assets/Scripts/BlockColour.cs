﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BlockColour : MonoBehaviour {

    BaseColours thisBlockColour;
    //Material mat;
    //BlockMover myBlockMover;
   // MaterialPropertyBlock block;
    //int colourID;
    //Renderer rend;

    UvColourChanger uvClrChanger;

    public BaseColours ThisBlockColour
    {
        get { return thisBlockColour; }
        set { thisBlockColour = value;
            SetBlockColour(thisBlockColour); }
    }

    private void Awake()
    {
        //mat = gameObject.GetComponent<MeshRenderer>().sharedMaterial;
        //myBlockMover = gameObject.GetComponent<BlockMover>();
        //block = new MaterialPropertyBlock();
        //rend = gameObject.GetComponent<Renderer>();
        //colourID = Shader.PropertyToID("_Color");
        uvClrChanger = gameObject.GetComponent<UvColourChanger>();
        uvClrChanger.Initialise();
    }

    /// <summary>
    /// Set the new colour for the gameObject
    /// </summary>
    /// <param name="newColour"></param>
    void SetBlockColour(BaseColours newColour)
    {
        //rend.GetPropertyBlock(block);

        //Color _newColour = Color.red;

        switch (newColour)
        {
            case BaseColours.Red:
                //already set - do nothing
                //myBlockMover.MyBaseColour = BaseColours.Red;
                uvClrChanger.ChangeEntityColour(BaseColours.Red);
                break;
            case BaseColours.Green:
                //_newColour = Color.green;
                //myBlockMover.MyBaseColour = BaseColours.Green;
                uvClrChanger.ChangeEntityColour(BaseColours.Green);
                break;
            case BaseColours.Blue:
                //_newColour = Color.blue;
                //myBlockMover.MyBaseColour = BaseColours.Blue;
                uvClrChanger.ChangeEntityColour(BaseColours.Blue);
                break;
            default:
                break;
        }

        //mat.color = _newColour;
       // block.SetColor(colourID, _newColour);
       // rend.SetPropertyBlock(block);
    }

    //void SetBlockColour(BaseColours newColour)
    //{
    //    Color _newColour = Color.red;

    //    switch (newColour)
    //    {
    //        case BaseColours.Red:
    //            //already set - do nothing
    //            myBlockMover.MyBaseColour = BaseColours.Red;
    //            break;
    //        case BaseColours.Green:
    //            _newColour = Color.green;
    //            myBlockMover.MyBaseColour = BaseColours.Green;
    //            break;
    //        case BaseColours.Blue:
    //            _newColour = Color.blue;
    //            myBlockMover.MyBaseColour = BaseColours.Blue;
    //            break;
    //        default:
    //            break;
    //    }

    //    mat.color = _newColour;
    //}




}

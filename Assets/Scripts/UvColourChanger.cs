﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UvColourChanger : MonoBehaviour {

    MaterialPropertyBlock block;
    Renderer rend;
    //Material mat;
    int textureID;
    Vector4 newOffset = new Vector4(1.0f, 1.0f, 0.0f, 0.0f);

    [SerializeField]BaseColours entityColour = BaseColours.Red;

    public void Initialise()
    {
        //mat = gameObject.GetComponent<MeshRenderer>().sharedMaterial;
        block = new MaterialPropertyBlock();
        rend = gameObject.GetComponent<Renderer>();
        textureID = Shader.PropertyToID("_MainTex_ST");
    }

    public BaseColours EntityColour
    {
        get { return entityColour; }
        set
        {
            entityColour = value;
            ChangeEntityColour(entityColour);
        }
    }

    public void ChangeEntityColour(BaseColours newColour)
    {
        if(block == null)
        {
            Initialise();
        }

        rend.GetPropertyBlock(block);

        switch (newColour)
        {
            case BaseColours.Red:
                newOffset.z = 0.0f;
                newOffset.w = 0.0f;
                break;
            case BaseColours.Green:
                newOffset.z = 0.25f;
                newOffset.w = 0.0f;
                break;
            case BaseColours.Blue:
                newOffset.z = 0.5f;
                newOffset.w = 0.0f;
                break;
            case BaseColours.Yellow:
                newOffset.z = 0.75f;
                newOffset.w = 0.0f;
                break;
            case BaseColours.Purple:
                newOffset.z = 0.0f;
                newOffset.w = -0.25f;
                break;
            case BaseColours.Aqua:
                newOffset.z = 0.25f;
                newOffset.w = -0.25f;
                break;
            default:
                break;
        }

        block.SetVector(textureID, newOffset);
        rend.SetPropertyBlock(block);
    }

}

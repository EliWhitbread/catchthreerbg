﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BlockMover : MonoBehaviour {

    [SerializeField]float moveSpeed, minRotSpeed, maxRotSpeed;
    [SerializeField] bool isLootBlock;
    //[SerializeField] int blockScoreValue;
    Transform myTransform;
    float rotSpeed;
    UvColourChanger colourChanger;

    public float MoveSpeed
    {
        get { return moveSpeed; }
        set { moveSpeed = value; }
    }
    
    private void OnEnable()
    {
        GameManager.curGameManager.OnMoveSpeedChange += CurGameManager_OnMoveSpeedChange;
        myTransform = this.transform;
        rotSpeed = Random.Range(minRotSpeed, maxRotSpeed);
        int _rotdir = Random.Range(0, 2);
        if(_rotdir == 0)
        {
            rotSpeed *= -1;
        }
        if(isLootBlock == false)
        {
            colourChanger = GetComponent<UvColourChanger>();
        }
        

    }

    private void CurGameManager_OnMoveSpeedChange(float _mSpeed)
    {
        moveSpeed = _mSpeed;
    }

    void Update()
    {

        myTransform.Translate(Vector3.down * (moveSpeed * Time.smoothDeltaTime), Space.World);
        myTransform.Rotate(Vector3.up * (rotSpeed * Time.smoothDeltaTime), Space.World);
        if (isLootBlock == true)
        {
            myTransform.Rotate(Vector3.right * (rotSpeed * Time.smoothDeltaTime), Space.Self);
        }

        if (myTransform.position.y <= -0.3f)
        {
            if (isLootBlock == true)
            {
                EffectSpawner.curEffectSpawner.SpawnEffect(SpawnableEffects.LootBlockDeath, myTransform);
                Destroy(this.gameObject);
            }
            else
            {
                EffectSpawner.curEffectSpawner.SpawnEffect(SpawnableEffects.DropBlockDeath, myTransform, colourChanger.EntityColour);
                gameObject.SetActive(false);
            }

        }

    }

    private void OnDisable()
    {
        GameManager.curGameManager.OnMoveSpeedChange -= CurGameManager_OnMoveSpeedChange;
        myTransform.position -= Vector3.left * 100;
    }

    //private void OnTriggerEnter(Collider other)
    //{
    //    if(other.CompareTag("Player"))
    //    {
    //        GameManager.curGameManager.CurrentScore = blockScoreValue;
    //    }
    //}

}

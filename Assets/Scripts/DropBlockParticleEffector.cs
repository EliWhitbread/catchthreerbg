﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DropBlockParticleEffector : MonoBehaviour {

    Color thisColour;
    //Material thisMat;
    ParticleSystemRenderer pRend;
    public BaseColours pColour;
    MaterialPropertyBlock block;
    int colourID;

    public BaseColours ParticleColour
    {
        set { pColour = value; SetEntityColour(pColour); }
    }

    private void Start()
    {
        //if (thisMat == null)
        //{
        //    thisMat = gameObject.GetComponent<ParticleSystemRenderer>().material;
        //}
        InitialiseEffect();
    }

    private void OnEnable()
    {
        //if (thisMat == null)
        //{
        //    thisMat = gameObject.GetComponent<ParticleSystemRenderer>().material;
        //    block = new MaterialPropertyBlock();
        //}

        if(pRend == null)
        {
            InitialiseEffect();
        }

        SetEntityColour(pColour);
    }

    void InitialiseEffect()
    {
        block = new MaterialPropertyBlock();
        colourID = Shader.PropertyToID("_Color");
        //colourID = Shader.PropertyToID("_TintColor");
        pRend = gameObject.GetComponent<ParticleSystemRenderer>();
    }

    public void SetEntityColour(BaseColours colr)
    {
        pRend.GetPropertyBlock(block);

        switch (colr)
        {
            case BaseColours.Red:
                thisColour = Color.red;
                break;
            case BaseColours.Green:
                thisColour = Color.green;
                break;
            case BaseColours.Blue:
                thisColour = Color.blue;
                break;
            default:
                thisColour = Color.green;
                break;
        }

        block.SetColor(colourID, thisColour);
        pRend.SetPropertyBlock(block);

        //thisMat.SetColor("_Color", thisColour);
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum BaseColours { Red, Green, Blue, Yellow, Purple, Aqua }

public class EntityColour : MonoBehaviour {
    
    //Color thisColour;
    //Material thisMat;
    public BaseColours playerColour = BaseColours.Red;
    UvColourChanger uvColour;
    //MaterialPropertyBlock block;
    //int colourID;
    //Renderer rend;

    //public Color ObjectColour
    //{
    //    get { return thisMat.color; }
    //}

    //private void Awake()
    //{
    //    SetDependencies();
    //}

    //void SetDependencies()
    //{
    //    thisMat = gameObject.GetComponent<MeshRenderer>().sharedMaterial;
    //    thisColour = thisMat.color;
    //    block = new MaterialPropertyBlock();
    //    rend = gameObject.GetComponent<Renderer>();
    //    colourID = Shader.PropertyToID("_Color");
    //}

    private void OnEnable()
    {
        //if (thisMat == null)
        //{
        //    SetDependencies();
        //}

        uvColour = gameObject.GetComponent<UvColourChanger>();

        if (gameObject.CompareTag("Player"))
        {
            ResetEntityColour();
        }

        
    }

    //public void AddEntityColours(Color refColour)
    //{
    //    rend.GetPropertyBlock(block);

    //    refColour.a = 0.0f;
    //    thisColour += refColour;

    //    if(thisColour.r > 1.0f || thisColour.g > 1.0f || thisColour.b > 1.0f)
    //    {
    //        CollectedWrongColour();
    //        return;
    //    }
    //    else if(thisColour.r > 0.0f && thisColour.g > 0.0f && thisColour.b > 0.0f)
    //    {
    //        AddCollectThreeBonus();
    //        return;
    //    }

    //    //thisMat.color = thisColour;
    //    block.SetColor(colourID, thisColour);
    //    rend.SetPropertyBlock(block);

    //}

    void ResetEntityColour()
    {
        if(uvColour == null)
        {
            uvColour = gameObject.GetComponent<UvColourChanger>();
        }
        playerColour = BaseColours.Red;

        int rnd = Random.Range(0, 3);
        switch (rnd)
        {
            case 0:
                //thisColour = Color.red;
                //playerColour = BaseColours.Red; - already red. do nothing.
                break;
            case 1:
                //thisColour = Color.green;
                playerColour = BaseColours.Green;
                break;
            case 2:
                //thisColour = Color.blue;
                playerColour = BaseColours.Blue;
                break;
            default:
                //thisColour = Color.green;
                playerColour = BaseColours.Green;
                break;
        }

        //thisMat.color = thisColour;
        //ChangeMeterialColour(thisColour);

        uvColour.EntityColour = playerColour;
    }

    void CollectedWrongColour(BaseColours playerColour, BaseColours blockColour)
    {
        GameManager.curGameManager.PlayerDied(false, playerColour, blockColour);
        gameObject.SetActive(false);
    }

    void AddCollectThreeBonus()
    {
        //GameManager.curGameManager.CurrentCrystals = 1;
        GameManager.curGameManager.UpdateScoreMultiplyer();
        int amt = 100 + (8 * GameManager.curGameManager.CurrentLootBlocks);
        //GameManager.curGameManager.CurrentScore = amt;
        EffectSpawner.curEffectSpawner.SpawnEffect(SpawnableEffects.CollectThreeEffect, this.transform);
        //GameManager.curGameManager.ShowScorePopup(false, amt);
        GameManager.curGameManager.UpdatePlayerScore(amt);
        if(GameManager.curGameManager.BonusCrystalUpgradeActive == true)
        {
            GameManager.curGameManager.ShowScorePopup(true);
            GameManager.curGameManager.CurrentLootBlocks = 1;
        }        
        ResetEntityColour();
    }

    //void ChangeMeterialColour(Color colr)
    //{
    //    rend.GetPropertyBlock(block);
    //    block.SetColor(colourID, colr);
    //    rend.SetPropertyBlock(block);
    //}
    
    //public void SetEntityColour(BaseColours colr)
    //{
    //    switch (colr)
    //    {
    //        case BaseColours.Red:
    //            thisColour = Color.red;
    //            playerColour = BaseColours.Red;
    //            break;
    //        case BaseColours.Green:
    //            thisColour = Color.green;
    //            playerColour = BaseColours.Green;
    //            break;
    //        case BaseColours.Blue:
    //            thisColour = Color.blue;
    //            playerColour = BaseColours.Blue;
    //            break;
    //        default:
    //            thisColour = Color.green;
    //            playerColour = BaseColours.Green;
    //            break;
    //    }

    //    //thisMat.color = thisColour;
    //    ChangeMeterialColour(thisColour);
    //}

    public void CombineEntityColours(BaseColours colr)
    {
        bool displayPopupScore = true;

        switch (playerColour)
        {
            case BaseColours.Red:
                if (colr == BaseColours.Red)
                {
                    //wrong coloured block collected
                    CollectedWrongColour(playerColour, colr);
                    displayPopupScore = false;
                }
                if(colr == BaseColours.Green)
                {
                    //_newColour = new Color(1.0f, 1.0f, 0.0f, 1.0f);
                    playerColour = BaseColours.Yellow;
                    //thisMat.color = _newColour;
                    //ChangeMeterialColour(_newColour);
                    uvColour.EntityColour = playerColour;
                }
                if (colr == BaseColours.Blue)
                {
                    //_newColour = new Color(1.0f, 0.0f, 1.0f, 1.0f);
                    playerColour = BaseColours.Purple;
                    //thisMat.color = _newColour;
                    //ChangeMeterialColour(_newColour);
                    uvColour.EntityColour = playerColour;
                }
                break;
            case BaseColours.Green:
                if (colr == BaseColours.Red)
                {
                    //_newColour = new Color(1.0f, 1.0f, 0.0f, 1.0f);
                    playerColour = BaseColours.Yellow;
                    //thisMat.color = _newColour;
                    //ChangeMeterialColour(_newColour);
                    uvColour.EntityColour = playerColour;
                }
                if (colr == BaseColours.Green)
                {
                    //wrong coloured block collected
                    CollectedWrongColour(playerColour, colr);
                    displayPopupScore = false;
                }
                if (colr == BaseColours.Blue)
                {
                    //_newColour = new Color(0.0f, 1.0f, 1.0f, 1.0f);
                    playerColour = BaseColours.Aqua;
                    //thisMat.color = _newColour;
                    //ChangeMeterialColour(_newColour);
                    uvColour.EntityColour = playerColour;
                }
                break;
            case BaseColours.Blue:
                if (colr == BaseColours.Red)
                {
                    //_newColour = new Color(1.0f, 0.0f, 1.0f, 1.0f);
                    playerColour = BaseColours.Purple;
                    //thisMat.color = _newColour;
                    //ChangeMeterialColour(_newColour);
                    uvColour.EntityColour = playerColour;
                }
                if (colr == BaseColours.Green)
                {
                    //_newColour = new Color(0.0f, 1.0f, 1.0f, 1.0f);
                    playerColour = BaseColours.Aqua;
                    //thisMat.color = _newColour;
                    //ChangeMeterialColour(_newColour);
                    uvColour.EntityColour = playerColour;
                }
                if (colr == BaseColours.Blue)
                {
                    //wrong coloured block collected
                    CollectedWrongColour(playerColour, colr);
                    displayPopupScore = false;
                }
                break;
            case BaseColours.Yellow:
                if (colr == BaseColours.Blue)
                {
                    AddCollectThreeBonus();
                }
                else
                {
                    //wrong coloured block collected
                    CollectedWrongColour(playerColour, colr);
                    displayPopupScore = false;
                }
                break;
            case BaseColours.Purple:
                if (colr == BaseColours.Green)
                {
                    AddCollectThreeBonus();
                }
                else
                {
                    //wrong coloured block collected
                    CollectedWrongColour(playerColour, colr);
                    displayPopupScore = false;
                }
                break;
            case BaseColours.Aqua:
                if (colr == BaseColours.Red)
                {
                    AddCollectThreeBonus();
                }
                else
                {
                    //wrong coloured block collected
                    CollectedWrongColour(playerColour, colr);
                    displayPopupScore = false;
                }
                break;
            default:
                break;
        }
        
        if(displayPopupScore == true)
        {
            int amt = EntitySpawner.curEntitySpawner.DropBlockScoreValue * GameManager.curGameManager.ScoreMultiplier;
            //GameManager.curGameManager.ShowScorePopup(false, amt);
            GameManager.curGameManager.UpdatePlayerScore(amt);
        }
    }


}

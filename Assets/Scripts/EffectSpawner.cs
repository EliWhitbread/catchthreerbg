﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum SpawnableEffects { CollectThreeEffect, DropBlockDeath, LootBlockDeath }

public class EffectSpawner : MonoBehaviour {

    public static EffectSpawner curEffectSpawner { get; private set; }

    public GameObject collectThreeEffectPrefab, dropBlockDeathEffect, lootBlockDeathEffect;
    public PopupText popupTextPrefab;
    public int effectPoolSize;

    List<GameObject> collectThreeEffectPool, dropBlockDeathEffectPool, lootBlockDeathEffectPool;
    List<PopupText> popupTextPool;

    private void Awake()
    {
        if(curEffectSpawner != null && curEffectSpawner != this)
        {
            Destroy(gameObject);
        }
        else
        {
            curEffectSpawner = this;
        }
    }

    private void Start()
    {
        collectThreeEffectPool = new List<GameObject>(effectPoolSize);

        for (int i = 0; i < effectPoolSize; i++)
        {
            GameObject obj = (GameObject)Instantiate(collectThreeEffectPrefab);
            obj.transform.position = Vector3.right * 20;
            //obj.SetActive(false);
            obj.GetComponent<EffectEnabler>().InitParticles();
            collectThreeEffectPool.Add(obj);
        }

        dropBlockDeathEffectPool = new List<GameObject>(effectPoolSize);

        for (int j = 0; j < effectPoolSize; j++)
        {
            GameObject obj = (GameObject)Instantiate(dropBlockDeathEffect);
            obj.transform.position = Vector3.right * 20;
            //obj.SetActive(false);
            obj.GetComponent<EffectEnabler>().InitParticles();
            dropBlockDeathEffectPool.Add(obj);
        }

        lootBlockDeathEffectPool = new List<GameObject>(effectPoolSize);

        for (int k = 0; k < effectPoolSize; k++)
        {
            GameObject obj = (GameObject)Instantiate(lootBlockDeathEffect);
            obj.transform.position = Vector3.right * 20;
            //obj.SetActive(false);
            obj.GetComponent<EffectEnabler>().InitParticles();
            lootBlockDeathEffectPool.Add(obj);
        }

        popupTextPool = new List<PopupText>();

        for (int l = 0; l < 3; l++)
        {
            PopupText obj = (PopupText)Instantiate(popupTextPrefab);
            obj.gameObject.transform.position = Vector3.right * 20;
            obj.gameObject.SetActive(false);
            popupTextPool.Add(obj);
        }
    }

    public void SpawnEffect(SpawnableEffects effect, Transform spnPosition, BaseColours colr = BaseColours.Red, bool moveToPlayer = false)
    {
        switch (effect)
        {
            case SpawnableEffects.CollectThreeEffect:
                SpawnCollectThreeEffect(spnPosition);
                break;
            case SpawnableEffects.DropBlockDeath:
                SpawnDropBlockDeathEffect(spnPosition, colr, moveToPlayer);
                break;
            case SpawnableEffects.LootBlockDeath:
                SpawnLootblockDeathEffect(spnPosition, moveToPlayer);
                break;
            default:
                break;
        }
    }

    void SpawnCollectThreeEffect(Transform spnPos)
    {
        for (int i = 0; i < collectThreeEffectPool.Count; i++)
        {
            EffectEnabler _enabler = collectThreeEffectPool[i].GetComponent<EffectEnabler>();
            if(_enabler.IsActive == false)
            {
                collectThreeEffectPool[i].transform.position = spnPos.position;
                collectThreeEffectPool[i].transform.rotation = Quaternion.identity;
                _enabler.Play();
                return;
            }            
        }
        
        GameObject obj = (GameObject)Instantiate(collectThreeEffectPrefab);
        obj.transform.position = spnPos.position;
        obj.transform.rotation = Quaternion.identity;
        collectThreeEffectPool.Add(obj);
        EffectEnabler effectEnabler = obj.GetComponent<EffectEnabler>();
        effectEnabler.InitParticles();
        effectEnabler.Play();
    }

    void SpawnDropBlockDeathEffect(Transform spnPos, BaseColours colr, bool followPlayer = false)
    {
        //for (int i = 0; i < dropBlockDeathEffectPool.Count; i++)
        //{
        //    if (!dropBlockDeathEffectPool[i].activeInHierarchy)
        //    {
        //        dropBlockDeathEffectPool[i].transform.position = spnPos.position;
        //        dropBlockDeathEffectPool[i].transform.rotation = Quaternion.identity;
        //        dropBlockDeathEffectPool[i].GetComponent<DropBlockParticleEffector>().ParticleColour = colr;
        //        if(followPlayer == true)
        //        {
        //            dropBlockDeathEffectPool[i].GetComponent<ParticlesMover>().TargetPlayerPos = true;
        //        }
        //        dropBlockDeathEffectPool[i].GetComponent<EffectEnabler>().Play();
        //        return;
        //    }
        //}

        for (int i = 0; i < dropBlockDeathEffectPool.Count; i++)
        {
            EffectEnabler _enabler = dropBlockDeathEffectPool[i].GetComponent<EffectEnabler>();
            if (_enabler.IsActive == false)
            {
                dropBlockDeathEffectPool[i].transform.position = spnPos.position;
                dropBlockDeathEffectPool[i].transform.rotation = Quaternion.identity;
                dropBlockDeathEffectPool[i].GetComponent<DropBlockParticleEffector>().ParticleColour = colr;
                if (followPlayer == true)
                {
                    dropBlockDeathEffectPool[i].GetComponent<ParticlesMover>().TargetPlayerPos = followPlayer;
                }
                _enabler.Play();
                return;
            }
        }

        GameObject obj = (GameObject)Instantiate(dropBlockDeathEffect);
        obj.transform.position = spnPos.position;
        obj.transform.rotation = Quaternion.identity;
        obj.GetComponent<DropBlockParticleEffector>().ParticleColour = colr;
        if (followPlayer == true)
        {
            obj.GetComponent<ParticlesMover>().TargetPlayerPos = followPlayer;
        }
        dropBlockDeathEffectPool.Add(obj);
        EffectEnabler effectEnabler = obj.GetComponent<EffectEnabler>();
        effectEnabler.InitParticles();
        effectEnabler.Play();
    }

    void SpawnLootblockDeathEffect(Transform spnPos, bool followPlayer = false)
    {
        //GameObject obj = (GameObject)Instantiate(lootBlockDeathEffect);
        //obj.transform.position = pos.position;
        //obj.transform.rotation = Quaternion.identity;

        for (int i = 0; i < lootBlockDeathEffectPool.Count; i++)
        {
            EffectEnabler _enabler = lootBlockDeathEffectPool[i].GetComponent<EffectEnabler>();
            if (_enabler.IsActive == false)
            {
                lootBlockDeathEffectPool[i].transform.position = spnPos.position;
                lootBlockDeathEffectPool[i].transform.rotation = Quaternion.identity;
                if (followPlayer == true)
                {
                    lootBlockDeathEffectPool[i].GetComponent<ParticlesMover>().TargetPlayerPos = followPlayer;
                }
                _enabler.Play();
                return;
            }
        }

        GameObject obj = (GameObject)Instantiate(lootBlockDeathEffect);
        obj.transform.position = spnPos.position;
        obj.transform.rotation = Quaternion.identity;
        lootBlockDeathEffectPool.Add(obj);
        EffectEnabler effectEnabler = obj.GetComponent<EffectEnabler>();
        effectEnabler.InitParticles();
        if (followPlayer == true)
        {
            obj.GetComponent<ParticlesMover>().TargetPlayerPos = followPlayer;
        }
        effectEnabler.Play();
    }
    
    public void SpawnPopupText(string msg, Vector3 pos)
    {
        for (int i = 0; i < popupTextPool.Count; i++)
        {
            if(!popupTextPool[i].gameObject.activeInHierarchy)
            {
                popupTextPool[i].DisplayMessage(msg);
                popupTextPool[i].gameObject.transform.position = pos;
                popupTextPool[i].gameObject.transform.rotation = Quaternion.identity;
                popupTextPool[i].gameObject.SetActive(true);
                popupTextPool[i].Show();
                return;
            }
        }

        PopupText obj = (PopupText)Instantiate(popupTextPrefab);
        obj.gameObject.transform.position = pos;
        obj.gameObject.transform.rotation = Quaternion.identity;
        obj.DisplayMessage(msg);
        obj.gameObject.SetActive(true);
        obj.Show();
        popupTextPool.Add(obj);

    }

    public void ClearAllEffects()
    {
        ClearEffectPool(ref collectThreeEffectPool);
        ClearEffectPool(ref dropBlockDeathEffectPool);
        ClearEffectPool(ref lootBlockDeathEffectPool);
    }

    void ClearEffectPool(ref List<GameObject> refList)
    {
        foreach (var effect in refList)
        {
            EffectEnabler enabler = effect.GetComponent<EffectEnabler>();
            if(enabler.IsActive == true)
            {
                enabler.Stop();
            }
        }
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class UIManager : MonoBehaviour {

    //public Text scoreText, crystalsText, scoreMultiplyerText, deathMessageText;
    public TextMeshProUGUI scoreText, crystalsText, scoreMultiplyerText, deathMessageText, lootBlocksToContinueText;
    public GameObject DeathPanel, extraLifeImage;
    string deathMessage = "Oh dear! That was the wrong colour.\nYour colour was {0}, and you just collected {1}";
    string continueString = "<sprite=0> x {0} to CONTINUE";
    public string[] playerFellMessages;

    string message = "";

    public string Message
    {
        get { return message; }
        private set { message = value; }
    }

    public void EnableDeathPanel(bool enablePanel)
    {
        DeathPanel.SetActive(enablePanel);
    }

    public void UpdateContinueButtonText(int amt)
    {
        lootBlocksToContinueText.text = string.Format(continueString, amt.ToString());
    }

    public void ToggleExtraLifeImage(bool state)
    {
        extraLifeImage.SetActive(state);
    }

    public void UpdateDeathMessage(BaseColours playerClr, BaseColours collectedClr, bool playerFell = false)
    {
        if(playerFell == true)
        {
            if(playerFellMessages.Length == 0)
            {
                message = "Oh, you fell off... okay";
                deathMessageText.text = message;
            }
            else
            {
                int rndMessge = (int)Random.Range(0, playerFellMessages.Length);
                message = playerFellMessages[rndMessge];
                deathMessageText.text = message;
            }
            return;
        }

        message = string.Format(deathMessage, playerClr.ToString(), collectedClr.ToString());
        deathMessageText.text = message;
    }

	public void UpdateScore(int value)
    {
        //scoreText.text = "SCORE: " + value.ToString();
        scoreText.text = value.ToString();
    }

    public void UpdateScoreMultiplyerText(int value)
    {
        scoreMultiplyerText.text = "X " + value.ToString();
    }

    //public void UpdateCrystalCountDisplay(int value)
    //{
    //    //crystalsText.text = "CRYSTALS: " + value.ToString();
    //    crystalsText.text = value.ToString();
    //}

    public void UpdateLootBlockCountDisplay(int value)
    {
        crystalsText.text = value.ToString();
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour {

    public static GameManager curGameManager { get; private set; }

    //block move speed
    public delegate void MoveSpeedChange(float _mSpeed);
    public event MoveSpeedChange OnMoveSpeedChange;

    public float minBlockMoveSpeed, maxBlockMoveSpeed, blockSpeedIncrementAmt, maxPlayerMoveDuration, minPlayerMoveDuration, playerMoveDurationDecrementAmt;

    public UIManager uiManager;
    public GameObject player;
    int currentScore, currentCrystals, currentlootBlocks = 0, totalLootBlocks, lootBlocksToContinue = 5;
    [SerializeField]
    int countsUntilMultiplyBoost = 3, scoreMultiplierStep = 0, scoreMultiplier = 1;
    [SerializeField]
    float currentBlockMoveSpeed, currentPlayerMoveDuration, timeAlive, blockSpawnDistWait;
    bool isAlive;
    int[] scoreMultiplyerSteps = { 2,3,5,8,13,21,34,55,89,144,233,377,610,987,1597,2584,4181,6765,10946 };
   // public PlayerSaveData playerSaveData;
    FadePanel fadePanel;

    //player upgrades
    [SerializeField]bool bonusCrystalUpgradeActive = false;
    bool bonusPointsUpgradeActive = false;
    [SerializeField]int playerLives = 1;

    //CameraMover camMover;

    public int PlayerLives
    {
        get { return playerLives; }
        set { playerLives = value; }
    }

    public bool BonusCrystalUpgradeActive
    {
        get { return bonusCrystalUpgradeActive; }
        set { bonusCrystalUpgradeActive = value; }
    }

    public bool BonusPointsUpgradeActive
    {
        get { return bonusPointsUpgradeActive; }
        private set { bonusPointsUpgradeActive = value; }
    }

    public int CurrentScore
    {
        get { return currentScore; }
    }

    public int ScoreMultiplier
    {
        get { return scoreMultiplier; }
    }

    //public int CurrentCrystals
    //{
    //    get { return currentCrystals; }
    //    set { currentCrystals += value; UpdatePlayerCrystals(); }
    //}

    public int CurrentLootBlocks
    {
        get { return currentlootBlocks; }
        set { currentlootBlocks += value; UpdatePlayerLootBlocks(); }
    }

    public FadePanel SetFadePanel
    {
        set { fadePanel = value; }
    }

    private void Awake()
    {
        if(curGameManager != null && curGameManager != this)
        {
            Destroy(gameObject);
        }
        else
        {
            curGameManager = this;
            DontDestroyOnLoad(gameObject);
        }
    }

    // Use this for initialization
    void Start () {
        
        if(player == null)
        {
            player = GameObject.FindGameObjectWithTag("Player");
        }
        InitialiseLevel(false);
	}

    private void Update()
    {
        if(isAlive == true)
        {
            timeAlive += Time.deltaTime;

            //if (timeAlive > 15.0f)
            //{
            //    timeAlive = 0;
            //    scoreMultiplyer++;
            //    uiManager.UpdateScoreMultiplyerText(scoreMultiplier);
            //}
        }
    }

    public void ContinueButtonAction()
    {
        if(CurrentLootBlocks >= lootBlocksToContinue)
        {
            CurrentLootBlocks = -lootBlocksToContinue;
            uiManager.UpdateLootBlockCountDisplay(CurrentLootBlocks);
            InitialiseLevel(true);
        }
        
    }

    public void InitialiseLevel(bool keepStats)
    { 
        player.SetActive(true);
        isAlive = true;
        timeAlive = 0.0f;
        scoreMultiplierStep = 0;
        scoreMultiplier = 1;

        if (keepStats == true)
        {
            //do nothing
        }
        else
        {
            //player data
            //try
            //{
            //    playerSaveData = JsonDataManager.dataManager.LoadPlayerData();

            //    if(playerSaveData.bestScore < currentScore)
            //    {
            //        playerSaveData.bestScore = currentScore;
            //    }
            //    if(playerSaveData.bestGameTime < timeAlive)
            //    {
            //        playerSaveData.bestGameTime = timeAlive;
            //    }

            //    playerSaveData.lootBlockCount = currentCrystals;


            //    JsonDataManager.dataManager.SavePlayerData(playerSaveData);

            //}
            //catch
            //{
            //    playerSaveData = new PlayerSaveData();
            //    playerSaveData.playerName = "NewPlayer";
            //    playerSaveData.lastScore = currentScore;
            //    playerSaveData.bestScore = currentScore;
            //    playerSaveData.lootBlockCount = currentCrystals;
            //    playerSaveData.lastGameTime = timeAlive;
            //    playerSaveData.bestGameTime = timeAlive;

            //    JsonDataManager.dataManager.SavePlayerData(playerSaveData);
            //}

            currentScore = 0;
            currentCrystals = 0;
            countsUntilMultiplyBoost = 3;
            uiManager.UpdateScore(currentScore);

            currentPlayerMoveDuration = maxPlayerMoveDuration;
            currentBlockMoveSpeed = minBlockMoveSpeed;

            PlayerMover.curPlayerMover.MoveDuration = currentPlayerMoveDuration;
            EntitySpawner.curEntitySpawner.BlockMoveSpeed = currentBlockMoveSpeed;
            EntitySpawner.curEntitySpawner.ResetSpawner();
        }
        
        PlayerMover.curPlayerMover.ResetPlayer();
        uiManager.EnableDeathPanel(false);
        EntitySpawner.curEntitySpawner.SpawnBlock();
        uiManager.UpdateScoreMultiplyerText(scoreMultiplier);
        EntitySpawner.curEntitySpawner.CanSpawn = true;
    }

    public void PlayerDied(bool playerFell = false, BaseColours playerColour = BaseColours.Red, BaseColours blockColour = BaseColours.Red)
    {
        EntitySpawner.curEntitySpawner.ClearBlocks();
        EffectSpawner.curEffectSpawner.ClearAllEffects();

        uiManager.UpdateDeathMessage(playerColour, blockColour, playerFell);

        if (playerLives > 0)
        {
            playerLives--;
            if(playerLives <= 0)
            {
                uiManager.ToggleExtraLifeImage(false);
            }
            fadePanel.Fade();
            StartCoroutine(WaitToInit());
            return;
        }

        BonusCrystalUpgradeActive = false;
        BonusPointsUpgradeActive = false;

        isAlive = false;
        EntitySpawner.curEntitySpawner.CanSpawn = false;
        lootBlocksToContinue = 5;
        uiManager.UpdateContinueButtonText(lootBlocksToContinue);
        uiManager.EnableDeathPanel(true);
        System.GC.Collect();
        
    }

    IEnumerator WaitToInit()
    {
        EntitySpawner.curEntitySpawner.CanSpawn = false;
        yield return new WaitForSeconds(0.7f);
        InitialiseLevel(true);
        yield return new WaitForSeconds(1.0f);
        EntitySpawner.curEntitySpawner.CanSpawn = true;
    }

    public void UpdatePlayerScore(int amt) //TODO - link this - all score updates through here.
    {
        if(BonusPointsUpgradeActive == true)
        {
            amt *= 2;
        }
        amt *= ScoreMultiplier;
        currentScore += amt;
        uiManager.UpdateScore(currentScore);
        ShowScorePopup(false, amt);
    }

    public void ShowScorePopup(bool isCollectThree = false, int scoreVal = 0)
    {
        string msg = isCollectThree == true ? "x1<sprite=0>" : scoreVal.ToString();
        EffectSpawner.curEffectSpawner.SpawnPopupText(msg, player.transform.position);
    }

    void UpdatePlayerLootBlocks()
    {
        uiManager.UpdateLootBlockCountDisplay(CurrentLootBlocks);
        int amt = EntitySpawner.curEntitySpawner.DropBlockScoreValue * scoreMultiplier;
        UpdatePlayerScore(amt);
    }

    public void UpdateScoreMultiplyer()
    {
        if (scoreMultiplierStep >= scoreMultiplyerSteps.Length)
        {
            return;
        }

        countsUntilMultiplyBoost--;
        if (countsUntilMultiplyBoost <= 0)
        {
            scoreMultiplier++;
            scoreMultiplierStep++;
            countsUntilMultiplyBoost = scoreMultiplyerSteps[scoreMultiplierStep];
            uiManager.UpdateScoreMultiplyerText(scoreMultiplier);
        }
    }

    public void UpdateLevelSpeed()
    {
        currentPlayerMoveDuration -= playerMoveDurationDecrementAmt;
        currentPlayerMoveDuration = Mathf.Clamp(currentPlayerMoveDuration, minPlayerMoveDuration, maxPlayerMoveDuration);

        currentBlockMoveSpeed += blockSpeedIncrementAmt;
        currentBlockMoveSpeed = Mathf.Clamp(currentBlockMoveSpeed, minBlockMoveSpeed, maxBlockMoveSpeed);
        
        PlayerMover.curPlayerMover.MoveDuration = currentPlayerMoveDuration;

        EntitySpawner.curEntitySpawner.BlockMoveSpeed = currentBlockMoveSpeed;

        OnMoveSpeedChange?.Invoke(currentBlockMoveSpeed);
       
    }
    
    public void ApplyPlayerUpgrade(int selection)
    {
        switch (selection)
        {
            case 1:
                if(currentlootBlocks >= 10 && BonusPointsUpgradeActive == false)
                {
                    currentlootBlocks -= 10;
                    BonusPointsUpgradeActive = true;
                }
                break;
            case 2:
                if(playerLives == 0 && currentlootBlocks >= 20)
                {
                    currentlootBlocks -= 20;                    
                    playerLives = 1;
                    uiManager.ToggleExtraLifeImage(true);
                }
                break;
            case 3:
                if (currentlootBlocks >= 30 && BonusCrystalUpgradeActive == false)
                {
                    currentlootBlocks -= 30;
                    BonusCrystalUpgradeActive = true;
                }
                break;
            default:
                break;
        }
        uiManager.UpdateLootBlockCountDisplay(CurrentLootBlocks);
        lootBlocksToContinue = 0;
        uiManager.UpdateContinueButtonText(lootBlocksToContinue);
    }
	
}

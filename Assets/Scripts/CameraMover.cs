﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraMover : MonoBehaviour {

    Transform camTransform, playerTransform;
    bool followPlayer = false;

    public Transform PlayerTransform
    {
        set { playerTransform = value; }
    }

    public bool FollowPlayer
    {
        set { followPlayer = value; }
    }

    private void Awake()
    {
        camTransform = this.transform;
    }

    // Update is called once per frame
    void LateUpdate () {
		
        if(followPlayer == false)
        {
            return;
        }

        if(playerTransform == null)
        {
            Debug.Log("Camera follow error: No player");
            return;
        }

        Vector3 pos = camTransform.position;
        pos.x = playerTransform.position.x;
        camTransform.position = pos;
	}

    public void ResetPosition()
    {
        Vector3 pos = camTransform.position;
        pos.x = playerTransform.position.x;
        camTransform.position = pos;
    }
}

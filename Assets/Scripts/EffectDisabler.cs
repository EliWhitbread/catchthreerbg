﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EffectDisabler : MonoBehaviour {

    private void OnEnable()
    {        
        StartCoroutine(DisableObject());
    }
    
    IEnumerator DisableObject()
    {
        yield return new WaitForSeconds(3);
        gameObject.SetActive(false);
    }
}

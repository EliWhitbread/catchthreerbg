﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ParticlesMover : MonoBehaviour {

    Transform target;
    float force, curTime;
    ParticleSystem ps;
    ParticleSystem.Particle[] myParticles;
    bool targetPlayerPos = false;
    float targetDistCheck = 0.7f;

	public bool TargetPlayerPos
    {
        set { targetPlayerPos = value; if (target == null) SetDependencies(); }
    }

    private void Start()
    {
        ps = GetComponent<ParticleSystem>();
        force = 10.0f;
    }

    private void SetDependencies()
    {
        if(target == null && GameManager.curGameManager.player.activeInHierarchy)
        {
            target = GameObject.FindGameObjectWithTag("Player").transform;
        }
    }

    private void OnDisable()
    {
        targetPlayerPos = false;
    }

    void FixedUpdate () {

        if(targetPlayerPos == false)
        {
            return;
        }
        
        myParticles = new ParticleSystem.Particle[ps.particleCount];
        ps.GetParticles(myParticles);
        float deltaForce = force * Time.deltaTime;
        for (int i = 0; i < myParticles.Length; i++)
        {
            if(target != null)
            {
                Vector3 dirToTarget = Vector3.Normalize(target.position - myParticles[i].position);
                Vector3 seekForce = dirToTarget * deltaForce;

                Vector3 offset = myParticles[i].position - target.position;
                float sqrLeng = offset.sqrMagnitude;

                //if (Vector3.Distance(myParticles[i].position, target.position) < 0.8f)
                //{
                //    myParticles[i].velocity = Vector3.zero;
                //}

                if (sqrLeng < targetDistCheck * targetDistCheck)
                {
                    myParticles[i].velocity = Vector3.zero;
                    myParticles[i].position = target.position;
                }
                else
                {
                    myParticles[i].velocity += seekForce;
                }
                
            }
            
        }
        
        ps.SetParticles(myParticles, myParticles.Length);
	}
    
}

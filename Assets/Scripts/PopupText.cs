﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class PopupText : MonoBehaviour
{
    TextMeshPro textMesh;
    float lifeTime = 1.5f, speedModifyer = 0;
    Transform myTransform;
    [SerializeField]Vector3 startPos, endPos;

    private void Awake()
    {
        if(textMesh == null)
        {
            textMesh = gameObject.GetComponent<TextMeshPro>();
        }
        myTransform = this.transform;
    }

    public void Show()
    {
        startPos = myTransform.position + new Vector3(Random.Range(-0.5f, 0.5f), Random.Range(0.6f, 1.5f), -0.5f);
        endPos = startPos + new Vector3(Random.Range(-2.0f, 2.0f), Random.Range(3.0f, 4.5f), Random.Range(-1.5f, 2.5f));
        speedModifyer = Random.Range(1.0f, 3.0f);
        StartCoroutine(MoveText());
    }

    public void DisplayMessage(string msg)
    {

        if (textMesh == null)
        {
            textMesh = gameObject.GetComponent<TextMeshPro>();
        }

        textMesh.text = msg;
        textMesh.alpha = 1.0f;
    }

    IEnumerator MoveText()
    {
        float _t = 0.0f;
        while(_t < lifeTime)
        {
            _t += Time.fixedDeltaTime;
            transform.position = Vector3.Lerp(startPos, endPos, (_t / 7) * speedModifyer);
            textMesh.alpha -= _t / 30;
            yield return null;
        }
        
        gameObject.SetActive(false);
    }
}

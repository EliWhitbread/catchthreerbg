﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(SpawnDataGenerator))]
public class SpawnDataEditor : Editor {

	public override void OnInspectorGUI()
    {
        DrawDefaultInspector();

        SpawnDataGenerator spawnDataGenerator = (SpawnDataGenerator)target;
        if(GUILayout.Button("Generate Spawn Data"))
        {
            spawnDataGenerator.GenerateData();
        }
    }
}
